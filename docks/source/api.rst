REST API
==========

Aplikacja posiada 3 endpointy:

- GET .../hello otwiera stronę "Hello Docker World"
- GET .../customers pobiera wszystkich klientów w bazie
- GET .../customers/{id} pobiera klienta o podanym id