Docker
==================

Docker służy do zastąpienia wirtualizacji za pomocą konteneryzacji. Umożliwia ona uruchomienie
wskazanych procesów aplikacji w wydzielonym do tego kontenerów. Każdy z nich posiada wydzielony obszar pamięci,
gdzie znajduje się zainstalowany obraz systemu, zależności i bibliotek potrzebnych do działania aplikacji.

Docker-compose
==================

Docker-compose pozwala na zarządzanie większą ilością kontenerów za pomocą pliku .yml i komend.
Automatyzuje on tworzenie i uruchamianie kontenerów co upraszcza pracę. Dzięki niemu nie występują także
błędy podczas uruchamiania kontenerów zależnych od siebie.
