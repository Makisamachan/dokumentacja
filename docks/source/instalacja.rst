Instalacja i uruchomienie aplikacji
==========================================

1. Instalacja docker i docker-compose lub sprawdzenie czy są zainstalowane.
2. Utowrzenie pliku docker-compose o rozszerzeniu .yaml lub .yml i umieszczenie go w folderze aplikacji.
3. Zbudowanie obrazu aplikacji za pomocą komendy::

    docker-compose build

4. Uruchomienie kontenerów::

    docker-compose up

