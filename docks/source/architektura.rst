Diagram klas
=================

.. uml::

    title Class Diagram

    class CustomerController {
        customerRepository: CustomerRepo
        +getCustomers()
        +getUserById(id: long)
    }
    interface CustomerRepo {
        findByLastName(lastName: String)
        findById(id: long)
    }
    class Customer {
        -id: Long
        -firstName: String
        -lastName: String
        +toString()
        +getId()
        +getFirstName()
        +getLastName()
    }

    CustomerController <|-down- CustomerRepo
    CustomerRepo o-down- Customer

Diagram przypadków użycia
=============================

.. uml::

    title Use Case Diagram

    left to right direction

    skinparam packageStyle rectangle
    skinparam actorStyle awesome

    actor :Client: as C
    actor :Database: as D

    rectangle App {
        (GET customer) <-- D
        (GET customer by ID) <-- D
        C --> (GET customer)
        C --> (GET customer by ID)
    }
